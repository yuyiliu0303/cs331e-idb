import json
from models import app, db, Event, City, Restaurant, Bar

# load json
def load_json(filename):
    """
        return a python dict jsn
        filename is a json file

    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()
        
    return jsn
    
# Create events
def create_events():
    """
        Event class has eight attributes
        id
        event_image - url of a related image
        short_title - shortened title for the event
        venue_city - city of venue from the venue response document
        venue_address - address of venue from the venue response document
        timezone - timezone of venue from the venue response document
        date - Date/time of the event in the local timezone of the venue
        type - type of event
    """
    events = load_json('backend/events.json')
    
    # Iterate through the json file to populate the table
    for event in events['events']:
        if event['venue']['country'] == 'US':
            id = event['id']
            event_image = event['performers'][0]['image']
            short_title = event['short_title']
            venue_city = event['venue']['city']
            venue_address = event['venue']['address']
            timezone = event['venue']['timezone']
            date = event['datetime_local']
            type = event['type']

            #print(id, short_title, venue_location, type)
            newEvent = Event(id = id, event_image = event_image, short_title = short_title, venue_city = venue_city, venue_address = venue_address, timezone = timezone, date = date, type = type)

            db.session.add(newEvent)
            db.session.commit()
        
def create_cities():
    """
        populate the book table
        using json file loaded from the API
    """
    cities = load_json('backend/cities.json')
    
    # Iterate through the json file to populate the table
    for city in cities['records']:
        id = city['recordid']
        name = city['fields']['name']
        county = city['fields']['county']
        state = city['fields']['state']
        pop_2010 = city['fields']['pop_2010']
        
        #print(id, short_title, venue_location, type)
        newCity = City(id = id, name = name, county = county, state = state, pop_2010 = pop_2010)
        
        db.session.add(newCity)
        db.session.commit()
        
def create_restaurants():
    restaurants = load_json('backend/restaurants.json')
    for restaurant in restaurants['businesses']:
        id = restaurant['id']
        name = restaurant['name']
        category = restaurant['categories'][0]['title']
        location = restaurant['location']['address1']
        rating = restaurant['rating']
        image_url = restaurant['image_url']
        review_count = restaurant['review_count']

        newRestaurant = Restaurant(id = id, name = name, category = category, image_url = image_url, location = location, rating = rating, review_count = review_count)
        
        db.session.add(newRestaurant)
        db.session.commit()
    
def create_bars():
    bars = load_json('backend/bars.json')
    for bar in bars:
        id = bar['ID']
        name = bar['Name']
        review_count = bar['Review Count']
        category = bar['Category']
        subcategory = bar['Sub Category']
        address = bar['Address']
        phonenumber = bar['Phone number']
        
        newBar = Bar(id = id, name = name, review_count = review_count, category = category, subcategory = subcategory, address = address, phonenumber = phonenumber)
        
        db.session.add(newBar)
        db.session.commit()

	

