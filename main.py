import os
from flask import Flask, render_template, request, send_from_directory, url_for, jsonify
from models import app, db, Event, Restaurant, City, Bar
from create_db import app
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import asc, desc, or_
from flask_cors import CORS
import json

per_page = 20

### Script below is only to be use to test REACT

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("splash.html")


@app.route('/api/books')
def book():
    event_list = db.session.query(Event).all()
    restaurant_list = db.session.query(Restaurant).all()
    city_list = db.session.query(City).all()
    #result_Event = str([book.short_title for book in event_list])
    result_Restaurant = str([book.name for book in restaurant_list])
    #result_City = str([book.name for book in city_list])
    return result_Restaurant

@app.route('/about/')
def about():
    return render_template('about.html')
    
@app.route('/restaurant/', methods=['GET','POST'])
def restaurant():
    page = request.args.get('page', 1, type=int)
    restaurants = db.session.query(Restaurant).paginate(page=page, per_page=per_page)
    if request.method == 'POST':
        if request.form['submit'] == 'Sort':
            sort = request.form.get('sortBy')
            order = request.form.get('order')
            if order == "ascending":
                restaurants = db.session.query(Restaurant).order_by(sort).paginate(page=page, per_page=per_page)
            else:
                restaurants = db.session.query(Restaurant).order_by(desc(sort)).paginate(page=page, per_page=per_page)
        elif request.form['submit'] == 'Search':
            search = request.form['searchBy']
            if search.isdecimal():
                restaurants = (
                    db.session.query(Restaurant)
                    .filter(or_(
                        Restaurant.name.like('%' + search + '%'),
                        Restaurant.category.like('%' + search + '%'),
                        Restaurant.location.like('%' + search + '%'),
                        Restaurant.review_count == int(search)))
                    .paginate(page=page, per_page=per_page)
                )
            else:
                restaurants = (
                    db.session.query(Restaurant)
                    .filter(or_(
                        Restaurant.name.like('%' + search + '%'),
                        Restaurant.category.like('%' + search + '%'),
                        Restaurant.location.like('%' + search + '%')))
                    .paginate(page=page, per_page=per_page)
                )
        else:
            pass
    return render_template('restaurant.html', restaurants = restaurants)
    
@app.route('/restaurant/<restaurant>/')
def onerestaurant(restaurant):
    restaurant = db.session.query(Restaurant).filter(Restaurant.name == str(restaurant)).first()
    return render_template('onerestaurant.html', single_restaurant=restaurant) 

@app.route('/event/', methods=['GET','POST'])
def event():
    page = request.args.get('page', 1, type=int)
    events = db.session.query(Event).paginate(page=page, per_page=per_page)
    if request.method == 'POST':
        if request.form['submit'] == 'Sort':
            sort = request.form.get('sortBy')
            order = request.form.get('order')
            if order == "ascending":
                events = db.session.query(Event).order_by(sort).paginate(page=page, per_page=per_page)
            else:
                events = db.session.query(Event).order_by(desc(sort)).paginate(page=page, per_page=per_page)
        elif request.form['submit'] == 'Search':
            search = request.form['searchBy']
            events = (
                db.session.query(Event)
                .filter(or_(
                    Event.short_title.like('%'+search+'%'),
                    Event.venue_city.like('%'+search+'%'),
                    Event.type.like('%'+search+'%')))
                .paginate(page=page, per_page=per_page)
            )
        else:
            pass

    return render_template('event.html', events = events)

@app.route('/event/<eventName>/')
def eventInstance(eventName):
    event = db.session.query(Event).filter(Event.short_title == str(eventName)).first()
    return render_template('eventInstance.html', event=event)

@app.route('/city/', methods=['GET','POST'])
def city():
    page = request.args.get('page', 1, type=int)
    cities = db.session.query(City).paginate(page=page, per_page=per_page)
    if request.method == 'POST':
        if request.form['submit'] == 'Sort':
            sort = request.form.get('sortBy')
            order = request.form.get('order')
            if order == "ascending":
                cities = db.session.query(City).order_by(sort).paginate(page=page, per_page=per_page)
            else:
                cities = db.session.query(City).order_by(desc(sort)).paginate(page=page, per_page=per_page)
        elif request.form['submit'] == 'Search':
            search = request.form['searchBy']
            if search.isdecimal():
                cities = (
                    db.session.query(City)
                    .filter(or_(
                        City.name.like('%' + search + '%'),
                        City.county.like('%' + search + '%'),
                        City.state.like('%' + search + '%'),
                        City.pop_2010 == int(search)))
                    .paginate(page=page, per_page=per_page)
                )
            else:
                cities = (
                    db.session.query(City)
                    .filter(or_(
                        City.name.like('%' + search + '%'),
                        City.county.like('%' + search + '%'),
                        City.state.like('%' + search + '%')))
                    .paginate(page=page, per_page=per_page)
                )
        else:
            pass

    return render_template('city.html', cities = cities)
    
@app.route('/city/<city>/')
def onecity(city):
    city = db.session.query(City).filter(City.name == str(city)).first()
    return render_template('onecity.html', city = city)


@app.route('/bars/', methods=['GET','POST'])
def bar():
    page = request.args.get('page', 1, type=int)
    bars = db.session.query(Bar).paginate(page=page, per_page=per_page)
    if request.method == 'POST':
        if request.form['submit'] == 'Sort':
            sort = request.form.get('sortBy')
            order = request.form.get('order')
            if order == "ascending":
                bars = db.session.query(Bar).order_by(sort).paginate(page=page, per_page=per_page)
            else:
                bars = db.session.query(Bar).order_by(desc(sort)).paginate(page=page, per_page=per_page)
        elif request.form['submit'] == 'Search':
            search = request.form['searchBy']
            bars = (
                db.session.query(Bar)
                .filter(or_(
                    Bar.name.like('%' + search + '%'),
                    Bar.review_count.like('%' + search + '%'),
                    Bar.category.like('%' + search + '%'),
                    Bar.subcategory.like('%' + search + '%'),
                    Bar.address.like('%' + search + '%'),
                    Bar.phonenumber.like('%' + search + '%')))
                .paginate(page=page, per_page=per_page)
            )
        else:
            pass
    return render_template('bars.html', bars = bars)
    
@app.route('/bars/<bar>/')
def oneBar(bar):
    bar = db.session.query(Bar).filter(Bar.name == str(bar)).first()
    return render_template('singleBar.html', singleBar=bar)

if __name__ == "__main__":

    app.run(debug=True)



