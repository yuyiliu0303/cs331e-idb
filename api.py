import requests
import json

def events():
    url = "https://api.seatgeek.com/2/events?per_page=100&client_id=MjE2NTcxNDd8MTYxNzIyMzU3My40NjU1MTMy&client_secret=baa0dd3958d118aeaacba90709c8511d7ec21f8741aa3909a5c63a5210e0a503"
    response = requests.request("GET", url)
    events_json = response.json()
    
    with open('backend/events.json', 'w') as outfile:
        outfile.write(json.dumps(response.json(), indent=4, sort_keys=True))
    print(response.status_code)
    #print(json.dumps(events_json, indent=4, sort_keys=True))
    
def cities():
    url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=cities-and-towns-of-the-united-states&q=&rows=50&facet=feature&facet=feature2&facet=county&facet=state&refine.state=TX"
    response = requests.request("GET", url)
    cities_json = response.json()
    with open('backend/cities.json', 'w') as outfile:
         outfile.write(json.dumps(response.json(), indent=4, sort_keys=True))
    print(response.status_code)
    #print(json.dumps(cities_json, indent=4, sort_keys=True))
    
def restaurants():
	api_key = '1N3EneLu-WdBG0JGPMTy0rhhWL93m_XIGGEFmVgT5-vXTjsGvNOzl-bv737yDvGN7Ol-HGRJgPmAtcc8o_aGsotaT5p3fSCJTOEqEwaz8apnNKJdGO6gRh9-depkYHYx'
	headers = {'Authorization': 'Bearer %s' % api_key}
	url = "https://api.yelp.com/v3/businesses/search"
	params = {'location':'Austin', 'limit': 50}

	response = requests.get(url, params=params, headers=headers)
	restaurants_json = response.json()
	with open('backend/restaurants.json', 'w') as outfile:
		outfile.write(json.dumps(response.json(), indent = 4, sort_keys = True))
	print(response.status_code)    
    
if __name__ == "__main__":
    cities()
    events()
    restaurants()

