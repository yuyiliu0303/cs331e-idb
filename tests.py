#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/main.py
# Fares Fraij
# ---------------------------

# -------
# imports
# -------

import os
import sys
import unittest
from models import db, Event, City, Restaurant, Bar


# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # Event
    # ---------

    # Insert
    def test_1(self):
        e = Event(id='ut1', event_image="ut1img", short_title="ut1title", venue_city="ut1city", venue_address="ut1address", timezone="ut1time", date="ut1date", type="ut1type")
        db.session.add(e)
        db.session.commit()

        q = db.session.query(Event).filter_by(id='ut1').one()
        self.assertEqual(str(q.event_image), "ut1img")
        self.assertEqual(str(q.short_title), "ut1title")
        self.assertEqual(str(q.venue_city), "ut1city")
        self.assertEqual(str(q.venue_address), "ut1address")
        self.assertEqual(str(q.timezone), "ut1time")
        self.assertEqual(str(q.date), "ut1date")
        self.assertEqual(str(q.type), "ut1type")

        db.session.query(Event).filter_by(id='ut1').delete()
        db.session.commit()

    def test_2(self):
        e = Event(id='ut2', event_image="ut2img", short_title="ut2title", venue_city="ut2city",
                  venue_address="ut2address", timezone="ut2time", date="ut2date", type="ut2type")
        db.session.add(e)
        db.session.commit()

        q = db.session.query(Event).filter_by(id='ut2').one()
        self.assertEqual(str(q.event_image), "ut2img")
        self.assertEqual(str(q.short_title), "ut2title")
        self.assertEqual(str(q.venue_city), "ut2city")
        self.assertEqual(str(q.venue_address), "ut2address")
        self.assertEqual(str(q.timezone), "ut2time")
        self.assertEqual(str(q.date), "ut2date")
        self.assertEqual(str(q.type), "ut2type")

        db.session.query(Event).filter_by(id='ut2').delete()
        db.session.commit()

    def test_3(self):
        e = Event(id='ut3', event_image="ut3img", short_title="ut3title", venue_city="ut3city",
                  venue_address="ut3address", timezone="ut3time", date="ut3date", type="ut3type")
        db.session.add(e)
        db.session.commit()

        q = db.session.query(Event).filter_by(id='ut3').one()
        self.assertEqual(str(q.event_image), "ut3img")
        self.assertEqual(str(q.short_title), "ut3title")
        self.assertEqual(str(q.venue_city), "ut3city")
        self.assertEqual(str(q.venue_address), "ut3address")
        self.assertEqual(str(q.timezone), "ut3time")
        self.assertEqual(str(q.date), "ut3date")
        self.assertEqual(str(q.type), "ut3type")

        db.session.query(Event).filter_by(id='ut3').delete()
        db.session.commit()

    # ---------
    # City
    # ---------

    # insert
    def test_4(self):
        c = City(id='ut4', name='ut4name', county='ut4county', pop_2010=0, state='ut4state')
        db.session.add(c)
        db.session.commit()

        q = db.session.query(City).filter_by(id='ut4').one()
        self.assertEqual(str(q.name), "ut4name")
        self.assertEqual(str(q.county), "ut4county")
        self.assertEqual(int(q.pop_2010), 0)
        self.assertEqual(str(q.state), "ut4state")

        db.session.query(City).filter_by(id='ut4').delete()
        db.session.commit()

    def test_5(self):
        c = City(id='ut5', name='ut5name', county='ut5county', pop_2010=1, state='ut5state')
        db.session.add(c)
        db.session.commit()

        q = db.session.query(City).filter_by(id='ut5').one()
        self.assertEqual(str(q.name), "ut5name")
        self.assertEqual(str(q.county), "ut5county")
        self.assertEqual(int(q.pop_2010), 1)
        self.assertEqual(str(q.state), "ut5state")

        db.session.query(City).filter_by(id='ut5').delete()
        db.session.commit()

    def test_6(self):
        c = City(id='ut6', name='ut6name', county='ut6county', pop_2010=2, state='ut6state')
        db.session.add(c)
        db.session.commit()

        q = db.session.query(City).filter_by(id='ut6').one()
        self.assertEqual(str(q.name), "ut6name")
        self.assertEqual(str(q.county), "ut6county")
        self.assertEqual(int(q.pop_2010), 2)
        self.assertEqual(str(q.state), "ut6state")

        db.session.query(City).filter_by(id='ut6').delete()
        db.session.commit()

    # ---------
    # Restaurant
    # ---------
    def test_7(self):
        r = Restaurant(id='ut7', name='ut7name', image_url='ut7image', category='ut7category', location='ut7location', rating=2, review_count=2)
        db.session.add(r)
        db.session.commit()

        q = db.session.query(Restaurant).filter_by(id='ut7').one()
        self.assertEqual(str(q.name), "ut7name")
        self.assertEqual(str(q.image_url), "ut7image")
        self.assertEqual(str(q.category), "ut7category")
        self.assertEqual(str(q.location), "ut7location")
        self.assertEqual(int(q.rating), 2)
        self.assertEqual(int(q.review_count), 2)

        db.session.query(Restaurant).filter_by(id='ut7').delete()
        db.session.commit()

    def test_8(self):
        r = Restaurant(id='ut8', name='ut8name', image_url='ut8image', category='ut8category', location='ut8location', rating=2,
                       review_count=2)
        db.session.add(r)
        db.session.commit()

        q = db.session.query(Restaurant).filter_by(id='ut8').one()
        self.assertEqual(str(q.image_url), "ut8image")
        self.assertEqual(str(q.name), "ut8name")
        self.assertEqual(str(q.category), "ut8category")
        self.assertEqual(str(q.location), "ut8location")
        self.assertEqual(int(q.rating), 2)
        self.assertEqual(int(q.review_count), 2)

        db.session.query(Restaurant).filter_by(id='ut8').delete()
        db.session.commit()

    def test_9(self):
        r = Restaurant(id='ut9', name='ut9name', image_url='ut9image', category='ut9category', location='ut9location', rating=2,
                       review_count=2)
        db.session.add(r)
        db.session.commit()

        q = db.session.query(Restaurant).filter_by(id='ut9').one()
        self.assertEqual(str(q.name), "ut9name")
        self.assertEqual(str(q.image_url), "ut9image")
        self.assertEqual(str(q.category), "ut9category")
        self.assertEqual(str(q.location), "ut9location")
        self.assertEqual(int(q.rating), 2)
        self.assertEqual(int(q.review_count), 2)

        db.session.query(Restaurant).filter_by(id='ut9').delete()
        db.session.commit()

    # ---------
    # Bar
    # ---------
    def test_10(self):
        b = Bar(id='ut10', name='ut10name', review_count='ut10review_count', category='ut10category', subcategory='ut10subcategory',
                       address='ut10address', phonenumber='ut10phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut10').one()
        self.assertEqual(str(q.name), "ut10name")
        self.assertEqual(str(q.review_count), "ut10review_count")
        self.assertEqual(str(q.category), "ut10category")
        self.assertEqual(str(q.subcategory), "ut10subcategory")
        self.assertEqual(str(q.address), "ut10address")
        self.assertEqual(str(q.phonenumber), "ut10phone")

        db.session.query(Bar).filter_by(id='ut10').delete()
        db.session.commit()

    def test_11(self):
        b = Bar(id='ut11', name='ut11name', review_count='ut11review_count', category='ut11category',
                subcategory='ut11subcategory',
                address='ut11address', phonenumber='ut11phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut11').one()
        self.assertEqual(str(q.name), "ut11name")
        self.assertEqual(str(q.review_count), "ut11review_count")
        self.assertEqual(str(q.category), "ut11category")
        self.assertEqual(str(q.subcategory), "ut11subcategory")
        self.assertEqual(str(q.address), "ut11address")
        self.assertEqual(str(q.phonenumber), "ut11phone")

        db.session.query(Bar).filter_by(id='ut11').delete()
        db.session.commit()

    def test_12(self):
        b = Bar(id='ut12', name='ut12name', review_count='ut12review_count', category='ut12category',
                subcategory='ut12subcategory',
                address='ut12address', phonenumber='ut12phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut12').one()
        self.assertEqual(str(q.name), "ut12name")
        self.assertEqual(str(q.review_count), "ut12review_count")
        self.assertEqual(str(q.category), "ut12category")
        self.assertEqual(str(q.subcategory), "ut12subcategory")
        self.assertEqual(str(q.address), "ut12address")
        self.assertEqual(str(q.phonenumber), "ut12phone")

        db.session.query(Bar).filter_by(id='ut12').delete()
        db.session.commit()


    def test_13(self):
        e = Event(id='ut13', event_image="ut13img", short_title="ut13title", venue_city="ut13city", venue_address="ut13address", timezone="ut13time", date="ut13date", type="ut13type")
        db.session.add(e)
        db.session.commit()

        q = db.session.query(Event).filter_by(id='ut13').one()
        self.assertEqual(str(q.event_image), "ut13img")
        self.assertEqual(str(q.short_title), "ut13title")
        self.assertEqual(str(q.venue_city), "ut13city")
        self.assertEqual(str(q.venue_address), "ut13address")
        self.assertEqual(str(q.timezone), "ut13time")
        self.assertEqual(str(q.date), "ut13date")
        self.assertEqual(str(q.type), "ut13type")

        db.session.query(Event).filter_by(id='ut13').delete()
        db.session.commit()

    def test_14(self):
        c = City(id='ut14', name='ut14name', county='ut14county', pop_2010=14, state='ut14state')
        db.session.add(c)
        db.session.commit()

        q = db.session.query(City).filter_by(id='ut14').one()
        self.assertEqual(str(q.name), "ut14name")
        self.assertEqual(str(q.county), "ut14county")
        self.assertEqual(int(q.pop_2010), 14)
        self.assertEqual(str(q.state), "ut14state")

        db.session.query(City).filter_by(id='ut14').delete()
        db.session.commit()

    def test_15(self):
        r = Restaurant(id='ut15', name='ut15name', image_url='ut15image', category='ut15category', location='ut15location', rating=2, review_count=2)
        db.session.add(r)
        db.session.commit()

        q = db.session.query(Restaurant).filter_by(id='ut15').one()
        self.assertEqual(str(q.name), "ut15name")
        self.assertEqual(str(q.image_url), "ut15image")
        self.assertEqual(str(q.category), "ut15category")
        self.assertEqual(str(q.location), "ut15location")
        self.assertEqual(int(q.rating), 2)
        self.assertEqual(int(q.review_count), 2)

        db.session.query(Restaurant).filter_by(id='ut15').delete()
        db.session.commit()

    #Christian Unit Tests
    def test_20(self):
        b = Bar(id='ut20', name='ut20name', review_count='ut20review_count', category='ut20category',
                subcategory='ut20subcategory',
                address='ut20address', phonenumber='ut20phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut20').one()
        self.assertEqual(str(q.name), "ut20name")
        self.assertEqual(str(q.review_count), "ut20review_count")
        self.assertEqual(str(q.category), "ut20category")
        self.assertEqual(str(q.subcategory), "ut20subcategory")
        self.assertEqual(str(q.address), "ut20address")
        self.assertEqual(str(q.phonenumber), "ut20phone")

        db.session.query(Bar).filter_by(id='ut20').delete()
        db.session.commit()

    #Christian Unit Tests
    def test_21(self):
        b = Bar(id='ut21', name='ut21name', review_count='ut21review_count', category='ut21category',
                subcategory='ut21subcategory',
                address='ut21address', phonenumber='ut21phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut21').one()
        self.assertEqual(str(q.name), "ut21name")
        self.assertEqual(str(q.review_count), "ut21review_count")
        self.assertEqual(str(q.category), "ut21category")
        self.assertEqual(str(q.subcategory), "ut21subcategory")
        self.assertEqual(str(q.address), "ut21address")
        self.assertEqual(str(q.phonenumber), "ut21phone")

        db.session.query(Bar).filter_by(id='ut21').delete()
        db.session.commit()

    #Christian Unit Tests
    def test_22(self):
        b = Bar(id='ut22', name='ut22name', review_count='ut22review_count', category='ut22category',
                subcategory='ut22subcategory',
                address='ut22address', phonenumber='ut22phone')
        db.session.add(b)
        db.session.commit()

        q = db.session.query(Bar).filter_by(id='ut22').one()
        self.assertEqual(str(q.name), "ut22name")
        self.assertEqual(str(q.review_count), "ut22review_count")
        self.assertEqual(str(q.category), "ut22category")
        self.assertEqual(str(q.subcategory), "ut22subcategory")
        self.assertEqual(str(q.address), "ut22address")
        self.assertEqual(str(q.phonenumber), "ut22phone")

        db.session.query(Bar).filter_by(id='ut22').delete()
        db.session.commit()






if __name__ == '__main__':
    unittest.main()
# end of code
