from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

# The variables below are in place for the postgres login
YourUserName = "postgres"
YourPassword = "Texas2021!"
YourHostname = "35.225.198.19"
YourDatabaseName = "postgres"


# To run locally 
#YourUserName = "postgres"
#YourPassword = "password"
#YourHostname = "localhost"
#YourDatabaseName = "postgres"



app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://'+YourUserName+':'+YourPassword+'@'+YourHostname+':5432/'+YourDatabaseName)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)


class Event(db.Model):
    """
        Event class has four attributes
        id
        event_image - url of a related image
        short_title - shortened title for the event
        venue_city - city of venue from the venue response document
        venue_address - address of venue from the venue response document
        timezone - timezone of venue from the venue response document
        date - Date/time of the event in the local timezone of the venue
        type - type of event
    """
    __tablename__ = 'events'
    
    id = db.Column(db.String(), primary_key=True)
    event_image = db.Column(db.String(), nullable=True)
    short_title = db.Column(db.String(), nullable=False)
    venue_city = db.Column(db.String(), nullable=False)
    venue_address = db.Column(db.String(), nullable=False)
    timezone = db.Column(db.String(), nullable=False)
    date = db.Column(db.String(), nullable=False)
    type = db.Column(db.String(), nullable=False)


class City(db.Model):
    """
        City class has five attributes
        recordid
        name
        county
        pop_2010
        state
    """
    __tablename__ = 'cities'
    
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    county = db.Column(db.String(), nullable=False)
    pop_2010 = db.Column(db.Integer, nullable=False) 
    state = db.Column(db.String(), nullable=False)
    
    
class Restaurant(db.Model):
    """
    Restaurant class has five attributes:
    id
    name
    category
    location
    review_count
    """
    __tablename__ = 'restaurants'
    id = db.Column(db.String(), primary_key = True)
    image_url = db.Column(db.String(), nullable = True)
    name = db.Column(db.String(), nullable=False)
    category = db.Column(db.String(), nullable=False)
    location = db.Column(db.String(), nullable=False)
    rating = db.Column(db.Integer(), nullable=False)
    review_count = db.Column(db.Integer(), nullable=False)
    
class Bar(db.Model):
    """
    Bar class has seven attributes:
    id
    name
    review count
    category
    subcategory
    address
    phone number
    """
    __tablename__ = 'bars'
    id = db.Column(db.String(), primary_key = True)
    name = db.Column(db.String(), nullable=False)
    review_count = db.Column(db.String(), nullable=False)
    category = db.Column(db.String(), nullable=False)
    subcategory = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=False)
    phonenumber = db.Column(db.String(), nullable=False)

    

